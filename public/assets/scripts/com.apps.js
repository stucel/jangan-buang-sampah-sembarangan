var _GLB = 'undefined' != typeof global ? global : window; if (!_GLB.Namespace) {_GLB.Namespace = function(name) {if (typeof name === 'string') {this.constructor.name = name;}return this;};_GLB.Namespace.prototype = {push: function(obj) {var $namespace = this;if (typeof obj === 'object' && !obj.length) {for (var key in obj) {if (obj.hasOwnProperty(key)) { $namespace[key] = obj[key]; }}}return this;},}}

(function (root, $) {
    /* Importing Stage Constructor */
    /* Creating Stage Constructor */
    var Stage = function () {
        return this;
    }

    /* Exporting Stage to Window */
    window.Stage = function () {
        return new Stage();
    }



    /* Importing Stage Modules */


    var Activator = {
        def: function () {}
    };
    var Destroyer = {
        def: function () {}
    };
    var Incoming = {
        def: function () {}
    };
    var Leaving = {
        def: function () {}
    };

    Activator.trashing = function () {
        var $this = this;

        setTimeout(function () {
            $('.bar', $this).each(function () {
                $(this).width($(this).attr('value') + '%' || 0);
            });
        }, 0);
    }
    Destroyer.trashing = function () {
        $('.bar', this).each(function () {
            $(this).width(0);
        });
    }

    Incoming.banjir = function () {
        var audio = $('audio', this)[0];
        audio.play();
    }
    Leaving.banjir = function () {
        var audio = $('audio', this)[0];
        audio.pause();
    }

    /* Initializing Documents */
    $(document).ready(function () {
        var allStages = $('.stage');

        $('.landing').addClass('ready');

        allStages.each(function (i) {
            var stage = $(this).attr('id', (i));

            var item = $('<span stageid="' + i + '" class="item">').appendTo('.stage-control');

            if (i == 0) {
                item.addClass('active');
            }

            item.click(function () {
                var scroll = $('.stage#' + i).offset();

                $('.stage-control .item').remClass('active');
                $(this).addClass('active');

                $('.stage .inner-frame').remClass('active');
                $('.stage#' + i + ' .inner-frame').addClass('active');

                $('html, body').animate({
                    scrollTop: scroll.top
                }, 400);
            });

            var scto = setTimeout(function () {}, 0);

            $(document).scroll(function (e) {
                if (($(document).scrollTop()) >= (stage.offset().top - window.innerHeight + 60) && $(document).scrollTop() <= (stage.offset().top + stage.offset().height - 60)) {
                    if (stage.hasAttr('entering')) {
                        Incoming[stage.attr('entering') || 'def'].call(stage.get(0));
                    }
                } else {
                    if (stage.hasAttr('entering')) {
                        Leaving[stage.attr('entering') || 'def'].call(stage.get(0));
                    }
                }
                if ($(document).scrollTop() >= stage.offset().top) {
                    $('.stage#' + (i - 1) + ' .inner-frame').addClass('overlapped');

                    if (screen.width < 768) {
                        $('.inner-frame', stage).addClass('').addClass('active');
                    } else {
                        $('.inner-frame', stage).addClass('pin').addClass('active');
                    }
                    item.addClass('active');

                    if (stage.hasAttr('activator')) {
                        Activator[stage.attr('activator') || 'def'].call(stage.get(0));
                    }
                } else if ($(document).scrollTop() >= (stage.offset().top - 100)) {
                    $('.inner-frame', stage).addClass('touching');
                } else {
                    $('.stage#' + (i - 1) + ' .inner-frame').remClass('overlapped');
                    $('.inner-frame', stage).remClass('pin').remClass('active').remClass('touching');
                    item.remClass('active');

                    if (stage.hasAttr('activator')) {
                        Destroyer[stage.attr('activator') || 'def'].call(stage.get(0));
                    }
                }
                clearTimeout(scto);

                scto = setTimeout(function () {}, 0);
            });
        });

        $('.scdown').each(function () {
            var parent = $(this).parent().parent();
            var id = Number(parent.attr('id')) || 0;

            if (id == 0) {
                $(this).click(function () {
                    var scroll = $('#' + (1)).offset();

                    console.log(id);

                    $('html, body').animate({
                        scrollTop: scroll.top
                    }, 400);
                });
            } else if (id) {
                $(this).click(function () {
                    var scroll = $('#' + (id + 1)).offset();

                    console.log(id);

                    $('html, body').animate({
                        scrollTop: scroll.top
                    }, 400);
                });
            }
        });
    });
})(window, jQuery);