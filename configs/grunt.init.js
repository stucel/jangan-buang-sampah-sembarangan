/* Creatign Config Namespace */
'@namespace configs';

/* Loading package informations */
var pkg = grunt.file.readJSON('package.json');

/* Loading task specific configs */
'@import task';

/* Initializing Grunt */
grunt.initConfig(configs);

/* Loading Plugins */
[
    'grunt-contrib-watch',
    'grunt-sass',
    'grunt-export',
    'grunt-swig-templates',
    'grunt-prettify',
    'grunt-sync',
].forEach(function (task) {
    grunt.loadNpmTasks(task);
});

/* Registering Tasks */
grunt.registerTask('default', [ 'exports', 'sass', 'sync', 'swig', 'prettify', 'watch' ]);

// Build only task (no watch)
grunt.registerTask('build', [ 'exports', 'sass', 'sync', 'swig', 'prettify' ]);
