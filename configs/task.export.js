'@namespace exports';

/* Libraries */
var libs = {
    options: {
        verbose: true
    },

    files: {
        'public/assets/scripts': [
            'source/assets/scripts/com.libs.js'
        ]
    }
}

/* Private Scripts */
var apps = {
    options: {
        verbose: true
    },

    files: {
        'public/assets/scripts': [
            'source/assets/scripts/com.apps.js'
        ]
    }
}
