'@namespace prettify';

/* Prettify Options */
var options = {
    indent: 4,
}

/* Prettify Page Files */
var main = {
    expand: true,
    cwd: 'public/',
    ext: '.html',
    src: ['**/*.html'],
    dest: 'public/'
}
