'@namespace sass';

// Development Task
var apps = {
    options: {
        sourceMap: true,
        outputStyle: 'compressed'
    },
    files: {
        'public/assets/styles/main.css': 'source/assets/styles/main.scss'
    }
}
