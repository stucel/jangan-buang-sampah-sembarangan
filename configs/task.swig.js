'@namespace swig';

// Getting Options.
'@import $root/source/data/data.swig.js';

// Core HTMLS
var cores = {
    expand: true,
    cwd: 'source/html',
    dest: 'public',
    src: ['*.swig'],
    ext: '.html'
}
