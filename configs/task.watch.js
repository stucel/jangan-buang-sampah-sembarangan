/* Watch Configs */
configs.watch = {
    options: {
        livereload: 2048
    },

    // Javascript - Libraries.
    jslib: {
        files: ['libraries/**/*.js'],
        tasks: ['exports:libs']
    },

    // Javascript - Applications.
    jsapp: {
        files: ['source/assets/scripts/**/*.js'],
        tasks: ['exports:apps']
    },

    // Javascript - Datas.
    datas: {
        files: ['source/data/**/*.js'],
        tasks: ['swig', 'prettify']
    },

    // SASS - Libraries.
    sasslib: {
        files: ['libraries/**/**.scss'],
        tasks: ['sass']
    },

    // SASS - Applications.
    sassapp: {
        files: ['source/assets/styles/**/*.scss'],
        tasks: ['sass:apps']
    },

    // Swig Templates
    htmlcores: {
        files: ['source/html/*.swig', 'source/html/**/*.swig'],
        tasks: ['swig', 'prettify']
    },

    // Font files
    fonts: {
        files: ['source/assets/fonts/**'],
        tasks: ['sync:fonts']
    },

    // Icon files
    icons: {
        files: ['source/assets/icons/**'],
        tasks: ['sync:icons']
    },

    // Image files
    images: {
        files: ['source/assets/images/**'],
        tasks: ['sync:images']
    },

}
