'@namespace sync';

// Sync fonts
var fonts = {
    files: [
        {
            cwd: 'source/assets/fonts',
            src: ['**'],
            dest: 'public/assets/fonts'
        }
    ],
    verbose: true,
    updateAndDelete: true
}

// Sync images
var images = {
    files: [
        {
            cwd: 'source/assets/images',
            src: ['**'],
            dest: 'public/assets/images'
        }
    ],
    verbose: true,
    updateAndDelete: true
}

// Sync icons
var icons = {
    files: [
        {
            cwd: 'source/assets/icons',
            src: ['**'],
            dest: 'public/assets/icons'
        }
    ],
    verbose: true,
    updateAndDelete: true
}