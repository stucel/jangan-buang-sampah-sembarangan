require('node-import');

/* Exporting Grunt Module */
module.exports = function(grunt) {
    /* Loading Time Loader */
    require('time-grunt')(grunt);

    /* Importing Grunt Initializer */
    imports.module('configs/grunt.init', { grunt: grunt });
}