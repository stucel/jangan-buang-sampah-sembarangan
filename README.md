# **Jangan Buang Sampah Sembarangan Development**

===============================

Ingat! Buanglah sampah pada tempatnya! :D

***
### **`Installation`**

Silahkan clone repo ini. Project ini menggunakan Grunt sebagai Compiler. Untuk dapat mengcompile project, pastikan NodeJS dan Grunt sudah terinstall. Usahakan perubahan tidak dilakukan di **`public/`** , karena akan di replace ketika di compile. Jadi usahakan folder output tetap bersih dan lakukan perubahan di dalam folder **`source/`**

- **Install NodeJS** // Buka link berikut, download dan install NodeJS.
 [http://nodejs.org](http://nodejs.org)

- **Install Grunt & Bower** //
Buka terminal dan jalankan perintah berikut:
    ```bash
    $ npm install -g grunt-cli
    ```
    atau
    ```bash
    $ sudo npm install -g grunt-cli
    ```

- **Install NodeJS Module** //
Install node_modules yang dibutuhkan untuk mengcompile project. Buka terminal dan cd ke path project. Misal /Users/name/project/janganbuangsampahsembarangan.git.

    ```bash
    $ npm install atau sudo npm install
    ```

- **Compile** //
Buka terminal **`source`** ke path project dan jalankan perintah ini:
    ```bash
    $ grunt
    ```
    Jika tidak terjadi kesalahan, maka outputnya akan seperti ini:
    ```bash
    $ Running "watch" task
	  Waiting...
    ```

### **`Catatan`**

Jika ingin melakukan live-editing, jangan tutup terminal yang menjalankan grunt tadi. Lakukan editing di dalam folder **`source`**, saat file di simpan Grunt akan otomatis mengcompile project. Jika halaman file yang di edit sedang di buka di browser, misal index.html yang ada di folder **`public`** , maka halaman otomatis reload.

### **`Struktur File`**

- **`root`**
  * **`configs`** - Configurasi template.
  * **`libraries`** - Libraries yang dibutuhkan untuk compile.
  * **`public`** - Hasil output compiler dari folder source.
  * **`source`** - Asset template website.

Untuk melakukan perubahan konten, silahkan edit file-file di folder **`source`**.