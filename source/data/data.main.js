'@namespace data';

/* Core Data */
var maintitle = 'Jangan Buang Sampah Sembarangan';
var url ='http://janganbuangsampahsembarangan.org'
var maindescription = 'Mengedukasi masyarakat agar sadar untuk selalu membuang sampah pada tempatnya.';
var keywords = 'jangan buang sampah sembarangan, buang sampah sembarangan, cara buang sampah, jakarta bersih, indonesia bersih';
var idpagefb = '189031444767675';

/* Icon Path */
var iconpath = 'assets/icons/appicon';

/* CSS Files */
var styles = {
    cwd: 'assets/styles',

    src: [
        'main.css'
    ]
}

/* Script Files */
var scripts = {
    cwd: 'assets/scripts',

    src: [
        'com.libs.min.js',
        'com.apps.min.js'
    ]
}

/* Importing page specific datas */
'@import stages/';

/* Stages */
var stages = [
    'footer',
    'twitter',
    'trash',
    'case',
    'proofing',
    'checking',
    'reason',
    'landing',
]