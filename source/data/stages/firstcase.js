'@namespace firstcase';

var title = 'Kerugian buang sampah sembarangan tuh apa aja sih ?';
var downTitle = 'Trus, kalo datanya gimana ?';

var cases = [
    {
        text: 'Bila dibuang dengan cara ditumpuk saja maka akan menimbulkan bau dan bakteri yang berbahaya.',
        icon: 'fly'
    },
    {
        text: 'Membuang sampah di sungai / kali dapat mengakibatkan pendangkalan yang demikian cepat.',
        icon: 'whouse'
    },
    {
        text: 'Cairan terhadap rembesan sampah yang masuk ke dalam drainase atau sungai akan mencemari air.',
        icon: 'biohazard'
    },
    {
        text: 'Buang sampah sembarangan bikin segala penjuru kota kotor. Wisatawan pada gak mau dateng lagi.',
        icon: 'headsick'
    }
]