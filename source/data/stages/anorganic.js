'@namespace anorganic';

/* Main Data */
var title = 'Sampah Anorganik';
var subtitle = 'Sampah anorganik yaitu sampah yang terdiri dari bahan-bahan yang sulit terurai secara biologis sehingga penghancurannya membutuhkan waktu sangat lama. Sampah anorganik berasal dari SDA yang tidak dapat diperbarui seperti mineral dan minyak bumi atau proses industri. Beberapa dari bahan ini tidak terdapat di alam seperti plastik dan aluminium. Sampah jenis ini biasanya berasal dari kebutuhan rumah tangga, misalnya berupa botol, botol plastik, tas plastik dan kaleng.';
var downTitle = '';
