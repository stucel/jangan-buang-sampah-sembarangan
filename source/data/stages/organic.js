'@namespace organic';

/* Main Data */
var title = 'Sampah Organik';
var subtitle = 'Sampah organik adalah limbah yang berasal dari sisa makhluk hidup (alam) seperti hewan, manusia, tumbuhan yang mengalami pembusukan atau pelapukan. Sampah ini tergolong sampah yang ramah lingkungan karena dapat di urai oleh bakteri secara alami dan berlangsungnya cepat, misalnya seperti sisa-sisa makanan, kotoran hewan, atau barang-barang yang bisa dimanfaatkan kembali seperti sampah kayu, kertas atau daun.';
var downTitle = '';
