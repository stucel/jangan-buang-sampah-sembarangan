'@namespace datacase';

/* Main Data */
title = 'Perilaku orang indonesia dalam membuang sampah';
downTitle = 'Wah gawat banget ya! Lalu gimana dong cara nyampah yang benar ?';

/* Statistik Perilaku */
var stats = [
    {
        title: 'Diangkut petugas/dibuang ke TPS/TPA',
        value: 84.74,
        color: 'white'
    },
    {
        title: 'Dibakar',
        value: 23.28,
        color: 'white'
    },
    {
        title: 'Dijual ke pengumpul barang bekas',
        value: 13.34,
        color: 'white'
    },
    {
        title: 'Ditimbun',
        value: 5.23,
        color: 'white'
    },
    {
        title: 'Dibuang ke laut/sungai/got',
        value: 4.63,
        color: 'white'
    },
    {
        title: 'Dibuang sembarangan',
        value: 3.86,
        color: 'white'
    },
    {
        title: 'Dibuat Kompos/Pupuk',
        value: 2.94,
        color: 'white'
    },
    {
        title: 'Didaur Ulang',
        value: 1.67,
        color: 'white'
    },
    {
        title: 'Cara Lainnya',
        value: 0.86,
        color: 'white'
    },
];

/* Statistik Pembuangan */
var qstats = [
    {
        title: 'Tumpukan sampah di wilayah DKI Jakarta per hari',
        value: '6.500 ton'
    },
    {
        title: 'Indonesia menduduki peringkat kedua penghasil sampah domestik per tahun',
        value: '5,4 juta ton'
    },
    {
        title: 'Rata - rata penduduk Indonesia menghasilkan sampah per hari',
        value: '2,5 liter'
    },
]