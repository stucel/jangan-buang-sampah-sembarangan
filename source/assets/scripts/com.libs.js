// Importing Libraries

/* jQuery */
'@import $root/libraries/jquery/dist/jquery.js';

/* GSAP */
'@import $root/libraries/gsap/src/uncompressed/TweenMax.js';
'@import $root/libraries/gsap/src/uncompressed/TimelineMax.js';

/* jQuery Patch */
'@import $root/libraries/jqpatch/dist/jqpatch.js';

/* CSSCript */
'@import $root/libraries/csscript/dist/csscript.jquery.js';

/* Instagram Feed */
'@import $root/libraries/instafeed.js/instafeed.js';
